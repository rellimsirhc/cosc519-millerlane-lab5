/* File:  hellothread.c
 * Author: CML
 * Created on 5 Oct, 2020, 
 *develop a C program, named hellothread.c, that takes an integer, n, at the command line that must be between 1-10 and uses Pthreads and implements 2 functions that do the following:
Checks the parameter to ensure the integer is between 1-10, if not it prints an error message and quits.
-Creates n threads
-Each thread is identified by an integer from 1-n, named id
-Upon creation, each thread is passed its id
-The main thread outputs “Creating thread #id” for each thread id
-For each thread, the entry point is the worker() function. */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <wait.h>
#include <pthread.h>
#include <inttypes.h>

//void worker()

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void* worker(void *arg)   //thread function
 {
 	int idt = *(int *)arg;
 
 	 printf("Hello World! I am thread # %d\n", idt);
		 
	pthread_exit(0); //terminate the threads
}

int main (int argc, char *argv[])
{	
int n = atoi(argv[1]); 	//number of threads to be created   
				//int n = *(int *)argv;

  while( n < 1 || n > 10)
   {
   printf("\n Please choose number between 1 and 10.\n");
   fflush(stdin);
   scanf ("%d", &n);
    }

pthread_t tid[10]; 	//id pthread array
pthread_attr_t attr; 	//set or thread attributes  	 

/*To create a new thread, the pthread_create function defined in pthread.h
is used, as follows: 
int pthread_create(pthread_t * thread, const pthread_attr_t * attr, 
void * (*start_routine)(void *), void *arg);*/
int ptr[10];  
  	
  	 for (int i = 1; i <= n; i++){
  		ptr[i-1] = i*1;  	//ptr array to ensure unique ptr to pass to worker
	 	pthread_create(&tid[i-1],NULL,worker,&ptr[i-1]); //(void *)thread creation &is address of operator
	 	
		printf("Creating Thread %d\n",i);
	}
	  
	  for (int i = 1; i <= n; i++){
	  	pthread_join(tid[i-1], NULL);    // main thread wait on others
	   	printf("\n");
	  exit(0);
	 }
     
     
 return 0;
}

//printf("\nYou entered:%d", n);
// pthread_create(&tid[i],NULL,worker,(void *)&tid[i]); //thread creation
 // printf("\n Creating Thread %d\n",*(int *)tid[i]);
   // printf ("\tTid=%lu",&tid[i]);

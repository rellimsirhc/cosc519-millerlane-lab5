#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <wait.h>

int main (int argc, char *argv[])
{
//int n;
int n = atoi(argv[1]);
//pid_t pid;

//n=5;
printf("\n collatz recieved %d\n", n);//verify reciev number from usecollatz
	 
 if (fork() == 0)  //when parent forks child pid for child is same as parent 0
 { 

 	while (n != 1) {
 	printf("%d", n);
 		if (n % 2 == 0) { 	//check if n is even
 			n = n/2;	//n even then divide by 2
      			printf("\teven\n");
    		} else if (n % 2 != 0){
       		n = (n*3)+1;  	// see if n is odd multiply by 3 and ad 1 
      			printf("\todd\n");
        	   }
 	}   
     	printf("1\n");
}else {
 	printf("START");
	printf("\tprocess id: %d\n",(int) getpid());
	wait(NULL); // wait for child process to finish
	printf("END");
	printf("\tprocess id: %d\n",(int) getpid());
}
 return 0;
}



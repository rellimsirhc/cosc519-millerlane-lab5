#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <unistd.h>
#include <wait.h>

int main ()
{
int n, child, status;

printf("Enter an interger between 1 and 100: ");
scanf ("%d", &n);
printf("\nYou entered: %d", n);

char str[5]; //pass character array

sprintf(str,"%d", n); //convert integer to string 

printf("\t to be passed %c", n);      
 
// int cpid;
int cpid=fork();    

printf("\t cpid=%d", cpid);

if (cpid == 0) {

	printf ("\n Executing Collatz-process= %d\t", (int) getpid());
	char *args[]={"./collatz",str, NULL}; //execution of collatz
        execvp(args[0],args);//passing arguments to collatz function 
    }
        printf ("\n parent from useollatz waiting \n");
    
    if ((child = waitpid (cpid, &status, 0)) == -1){  //compare the 
         printf ("\n parent:error, usecollatz\n");
}
    if (child == cpid){
        printf ("\n Child waited for by parent in usecollatz.\n");
        }
     
 return 0;
}

  //int c=fork();//fork call
 //  if(c==0)//child process
//   {
//      execve ("collatz", userargv);  //call to program as child process       
 //  }
//}


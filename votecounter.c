#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

int votes = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *countvotes(void *param) {
  int i;
 //int mut = *(int *) param;
  for (i = 0; i < 10000000; i++) {
  	pthread_mutex_lock(&mutex); //lock access to vote
  	votes += 1;
  //  mut=votes;
  	pthread_mutex_unlock(&mutex);  // unlock access to vote
  }
    pthread_exit(NULL);
  return NULL;
}

int main() {
  pthread_t tid1, tid2, tid3, tid4, tid5;
  //int mut1 = 1;
  //pthread_create(&tid1, NULL, countvotes, &mut1);
  pthread_create(&tid1, NULL, countvotes, NULL);
  
  //int mut2=1;
 // pthread_create(&tid2, NULL, countvotes, &mut2);
  pthread_create(&tid2, NULL, countvotes, NULL);
  
  pthread_create(&tid3, NULL, countvotes, NULL);
  pthread_create(&tid4, NULL, countvotes, NULL);
  pthread_create(&tid5, NULL, countvotes, NULL);

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  pthread_join(tid5, NULL);

  printf("Vote total is %d\n", votes);
  return 0;
}
